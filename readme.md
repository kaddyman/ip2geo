# Python script for MaxMind geoIP service 

A script that wraps around the GeoIP2 webservice and minFraud Score. 
If you have an account with maxmind this will allow you to check geo/Fraud 
ips in your terminal. The script will return text results by default or 
json results with the `--json` flag.

## Install

`pip3 install -r requirements.txt`

## bashrc function

Be sure to add your userID and key as defaults in the script or in the bash functions 
in your bashrc/bash_profile files.

```bash
# return results as text
function iplook() {
  ${HOME}/repos/gitlab/ip2geo/ip2geo --maxuserid <userid> --maxkey <key> --ip "${1:-}"
}

# return results in json
function iplookj() {
  ${HOME}/repos/gitlab/ip2geo/ip2geo --maxuserid <userid> --maxkey <key> --ip "${1:-}" --json
}
```

## Notes

This will output all fields available for insight queries and minfraud results for ips. These lookups are pretty expensive against your credits so make sure to use them only when needed.


Yes it almost killed me to write out this many if/else statements but the returned payload from the api isn't json/dict (it's a model class) so this was the only way to format the output. I did it so you don't have to. :P


Maxminds GeoIP2 Python API: https://github.com/maxmind/GeoIP2-python


Maxminds FraudScore API: https://github.com/maxmind/minfraud-api-python


Example:
```
 $ ./ip2geo --ip 137.116.78.48

continent_name: North America
continent_code: NA
continent_geoname_id: 6255149

country_name: United States
country_confidence: 99
country_geoname_id: 6252001
country_is_in_european_union: False
country_iso_code: US

city_name: Boydton
city_confidence: 0
city_geoname_id: 4748634

location_average_income: NA
location_accuracy_radius: 1000KM
location_latitude: 36.6544
location_longitude: -78.3752
location_metro_code: 560
location_population_density: NA
location_time_zone: America/New_York

postal_code: 23917
postal_confidence: 0

subdivision_name: Virginia
subdivisions_confidence: 0
subdivisions_geoname_id: 6254928
subdivisions_iso_code: VA

autonomous_system_number: 8075
autonomous_system_organization: Microsoft Corporation
connection_type: NA
domain: NA
ip_address: 137.116.78.48
is_anonymous: True
is_anonymous_proxy: False
is_anonymous_vpn: False
is_hosting_provider: True
is_legitimate_proxy: False
is_public_proxy: False
is_satellite_provider: False
is_tor_exit_node: False
isp: Microsoft Corporation
organization: Microsoft Azure
user_type: hosting

score_risk_score [0.01-99]: 54
score_ip_address [0.01-99]: ScoreIPAddress(risk=45)

queries_remaining: 90526
```
